from django.contrib import admin

from django.contrib.gis.admin import OSMGeoAdmin
from .models import *


@admin.register(BluetoothTagUser)
class UserAdmin(admin.ModelAdmin):
    list_display = ('DATE_OF_BIRTH', 'IMAGE_PROFILE', 'FIRST_NAME', 'LAST_NAME', \
        'GENDER', 'HOME_ADDR', 'USER_NAME', 'PASSWORD', 'COVID_19_STATUS', 'COVID_19_LINEAGE')

@admin.register(BluetoothTagData)
class BluetoothTagDataAdmin(OSMGeoAdmin):
    list_display = ('TIMESTAMP', 'BT_TAG_DEVICE_NAME', 'BT_TAG_OWNER', 'X_COORD', 'Y_COORD', 'LOCATION', \
        'LATITUDE_LONGTITUDE', 'FLOOR', 'ROOM')
    list_filter = ('TIMESTAMP',)


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('ID', 'TIMESTAMP', 'USER_1', 'COVID_19_STATUS_USER_1', 'USER_2', 'COVID_19_STATUS_USER_2', 'DISTANCE',
        'CONTACT_TRACING_LEVEL')