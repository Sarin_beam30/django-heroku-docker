import json, requests
from django.http.response import HttpResponse
from channels.generic.websocket import AsyncWebsocketConsumer

class RealtimeLocationConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.channel_group_name = 'location-realtime-data'

        # Join room group
        await self.channel_layer.group_add(
            self.channel_group_name,
            self.channel_name
        )
        await self.accept()
        print("----------***** (REALTIME_LOCATION_CONSUMER) connected  *****----------")
    
    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.channel_group_name,
            self.channel_name
        )
        print("(REALTIME_LOCATION_CONSUMER)  DISCONNECED CODE: ", close_code)
        print("---------- (REALTIME_LOCATION_CONSUMER) disconnect--------")
    

    # Receive message from WebSocket
    async def receive(self, text_data=None, bytes_data=None):
        print("(REALTIME_LOCATION_CONSUMER) RECEIVE : " , text_data)
        data = json.loads(text_data)
        message = data["payload"]
        await self.channel_layer.group_send(
            self.channel_group_name,
            {
                "type": 'log_message',
                "payload": message
            }
        )
        print("---------- (REALTIME_LOCATION_CONSUMER) receive--------")

    async def log_message(self, event):
        # print("(REALTIME_LOCATION_CONSUMER) EVENT : " + str(event))
        message = event["payload"]
        await self.send(text_data=json.dumps({
            "payload": message
        }))

        print("----------- (REALTIME_LOCATION_CONSUMER) LOG ------------")

class ContactConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.channel_group_name = 'contact-data'

        # Join room group
        await self.channel_layer.group_add(
            self.channel_group_name,
            self.channel_name
        )
        await self.accept()
        print("----------***** (CONTACT_CONSUMER)  connected *****----------")
    
    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.channel_group_name,
            self.channel_name
        )
        print("(CONTACT_CONSUMER)  DISCONNECED CODE: ", close_code)
        print("---------- (CONTACT_CONSUMER) disconnect--------")
    
    # Receive message from WebSocket
    async def receive(self, text_data=None, bytes_data=None):
        print("(CONTACT_CONSUMER) Receive : " + text_data)
        data = json.loads(text_data)
        message = data["contact_payload"]
        await self.channel_layer.group_send(
            self.channel_group_name,
            {
                "type": "contact_message",
                "contact_payload": message
            }
        )
        print("------------ (CONTACT_CONSUMER) receive ------------")
    
    async def contact_message(self, event):
        # print("(CONTACT_CONSUMER) EVENT : " + str(event))
        message = event["contact_payload"]
        await self.send(text_data=json.dumps({
            "contact_payload": message
        }))

        print("---------------(CONTACT_CONSUMER) LOG------------")