#CELERY
from json.decoder import JSONDecodeError
from celery import shared_task
from celery.schedules import *

#DJANGO
import django, datetime
from django.db import models
from django.db.models.query import QuerySet
from senior_software_project_backend_side.models import BluetoothTagData, BluetoothTagUser, Contact
from django.core import serializers
from django.contrib.gis.geos import Point, GEOSGeometry
from django.utils import timezone
from django.http import JsonResponse
from django.db.models import fields

#EXTERNAL_LIB
from datetime import datetime
from pytz import timezone
import requests, socket, json, ast, websocket

#CHANNEL
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

import traceback
from psycopg2 import errors, IntegrityError

class JsonEncoder(json.JSONEncoder):
    def default(self, o):
        # print("TEST JsonEncoder : " , o)
        if isinstance(o, datetime):
            now_thailand = o.astimezone(timezone('Asia/Bangkok'))
            return now_thailand.strftime("%d-%m-%Y %H:%M:%S")
        
        if isinstance(o, Point):
            return o.geojson

        print("TYPE : " , type(dict(json.JSONEncoder.default(self, o))))
        return dict(json.JSONEncoder.default(self, o))
        


def calculateDistanceBetweenTwoTags(x1, y1, x2, y2):    
    p1 = Point(x1, y1, srid=3857)
    p2 = Point(x2, y2, srid=3857)
    # print("DISTANCE : " + str(p1.distance(p2)))
    return p1.distance(p2)

def setContractacingLevel(distance):
    # distance > 15 meters
    if(distance > 15):
        return 0
    # distance 10-15 meters
    if (distance >= 10 and distance <= 15):
        return 1
    # distance 3-9 meters
    elif(distance > 3 and distance <= 9):
        return 2
    # distance 0-3 meters
    elif(distance >= 0 and distance <= 3 ):
        return 3

def testDistanceFunction():
    # In our case, 1 degree equal to 1 meter
    #SRID=3857 
    #SRID=900913 in meters
    # p1 = Point(13.72788, 100.77073, srid=3857)
    p1 = Point(2.419000, 0.842400, srid=3857)
    # p2 = Point(13.72774, 100.77073, srid=3857)
    p2 = Point(1.270900, -0.128400, srid=3857)
    # 1 degrees = 111k meter
    print("TEST_DIS_FUNC (Degrees) : " + str(p1.distance(p2)))
    print("TEST_DIS_FUNC (meters)  : " + str(p1.distance(p2)*111000))

def lower_keys(json):
    if isinstance(json, list):
        return [lower_keys(v) for v in json]
    elif isinstance(json, dict):
        return dict((k.lower(), lower_keys(v)) for k, v in json.items())
    else:
        return json

def channelSendMethodBluetoothTagData(username):
    # QUERY_DATA_FROM_DATABASE
    latestListModelObject = BluetoothTagData.objects.all()\
        .filter(BT_TAG_OWNER=username)\
        .values('BT_TAG_DEVICE_NAME', 'BT_TAG_OWNER', 'TIMESTAMP','X_COORD', 'Y_COORD', 'LOCATION', 'LATITUDE_LONGTITUDE','FLOOR', 'ROOM')\
        .latest('TIMESTAMP')
    latestListModelString = json.dumps(latestListModelObject, cls=JsonEncoder)
    latestListModelJson = json.loads(latestListModelString)
    latestListModellowerCaseJson = lower_keys(latestListModelJson)
    print("latestListModelJson : " , (latestListModellowerCaseJson))

    # SEND_DATA_BY_WEB_SOCKET
    channel_group_name = 'location-realtime-data'
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        channel_group_name,
        {
            "type" : "log_message",
            "payload" : latestListModellowerCaseJson
        }
    )


# 3 case --> 1) Do nothing (NO_USER_LOGIN) 2) query only user 3) query all user (ADMIN_MODE)
@shared_task
def sendLastCurrentLocationToFrontend():
    # INIT_VARIABLE
    currentUserList = []
    
    # GET_CURRENT_USER_INFORMATION
    hostnameProduction = "http://127.0.0.1:8080/getAllCurrentUser/"
    hostnameHeroku = "https://protected-brook-89084.herokuapp.com/getAllCurrentUser/"
    r = requests.get(hostnameHeroku)
    current_user_information = r.content.decode("utf-8")
    print('(CURRENT_USER_INFORMATION) DATA : ' + str(current_user_information))
    
    if(current_user_information == "{}"):
        print("No user has login in the system")
    
    elif(current_user_information != None):
        try:
            # Convert current_user_information from string to Json
            current_user_information_json = json.loads(current_user_information)

            # Access value of Json
            for key, value in current_user_information_json.items():
                currentUserList.append(value)

            # Loop in currentUserList
            print(currentUserList)
            for username in currentUserList:
                channelSendMethodBluetoothTagData(username)
                
        except(JSONDecodeError):
            print("[sendLastCurrentLocationToFrontend] raise JSONDecodeError(\"Expecting value\", s, err.value) from None ")

    print('----------- *** (BLUETOOTH_TAG_DATA) SEND_DATA_ALREADY *** ----------- ')

oldCountContactModel = 0
@shared_task
def sendContactToFrontend():
    global oldCountContactModel
    countContactModel = Contact.objects.all().count()

    if(oldCountContactModel != 0):
        if(countContactModel > oldCountContactModel):
            print("--- (CONTACT) Contact Model was updated ---")

    contactModelObject = Contact.objects.all() \
        .values('TIMESTAMP', 'USER_1', 'COVID_19_STATUS_USER_1', 'USER_2', 'COVID_19_STATUS_USER_2','CONTACT_TRACING_LEVEL') 
    contactModelObjectList = list(contactModelObject)

    contactModelObjectString = json.dumps(contactModelObjectList, cls=JsonEncoder)
    contactModelObjectJson = json.loads(contactModelObjectString)
    contactModelObjectlowercaseJson = lower_keys(contactModelObjectJson)
    
    print("(CONTACT) Len : ", countContactModel)
    # print("(CONTACT) JSON : " , contactModelObjectlowercaseJson)

    channel_group_name = 'contact-data'
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        channel_group_name,
        {
            "type" : "contact_message",
            "contact_payload" : contactModelObjectlowercaseJson
        }
    )
    print('----------- *** (CONTACT) SEND_DATA_ALREADY *** ----------- ')
    oldCountContactModel = countContactModel
    print("(CONTACT) CURRENT_DATA_ROW : ", oldCountContactModel)

first_index_in_bluetooth_tag_data = bluetoothTagData_len = 0
IsrunWorkerForTheFirstTime = True
@shared_task
def updateContactModel():
    global IsrunWorkerForTheFirstTime
    global first_index_in_bluetooth_tag_data
    global bluetoothTagData_len
    UniqueViolation = errors.lookup('23505')  # Correct way to Import the psycopg2 errors

    print('-----*****   [updateContactModel] Function [Append the data]  *****-----')
    contactDataLen = Contact.objects.all().count()
    print("[updateContactModel] contactDataLen : " + str(contactDataLen))

    if(IsrunWorkerForTheFirstTime == True):
        Contact.objects.all().delete()
        contactDataLen = Contact.objects.all().count()
        print("[updateContactModel][IN_CONDITION] contactDataLen : " + str(contactDataLen))

        # SET IsrunWorkerForTheFirstTime to false
        IsrunWorkerForTheFirstTime = False
    
    elif(IsrunWorkerForTheFirstTime == False):
        bluetoothTagData_len = BluetoothTagData.objects.all().values('BT_TAG_DEVICE_NAME', 'BT_TAG_OWNER','X_COORD', 'Y_COORD').count()
        print('BTD_LEN : ' + str(bluetoothTagData_len))
        print('FIRST_INDEX_IN_BLUETOOTH_TAG_DATA : ' + str(first_index_in_bluetooth_tag_data))  
        contactTracingLevel = 0
        bluetoothTagData = (BluetoothTagData.objects.all()\
            .values('TIMESTAMP', 'BT_TAG_DEVICE_NAME', 'BT_TAG_OWNER','X_COORD', 'Y_COORD', 'LOCATION', 'LATITUDE_LONGTITUDE', 'FLOOR', 'ROOM')\
                [first_index_in_bluetooth_tag_data:bluetoothTagData_len])
                 
        try:
            for i in range(len(bluetoothTagData)-1):
                # print("DATA : " , bluetoothTagData[i])
                print("INDEX [i] : " + str(i+1))
                if(bluetoothTagData[i]['LOCATION'] == bluetoothTagData[i+1]['LOCATION'] \
                    and bluetoothTagData[i]['LATITUDE_LONGTITUDE'] == bluetoothTagData[i+1]['LATITUDE_LONGTITUDE'] \
                    and bluetoothTagData[i]['FLOOR'] == bluetoothTagData[i+1]['FLOOR'] \
                    and bluetoothTagData[i]['ROOM'] == bluetoothTagData[i+1]['ROOM']):
                    
                    if(bluetoothTagData[i]['BT_TAG_DEVICE_NAME'] != bluetoothTagData[i+1]['BT_TAG_DEVICE_NAME'] \
                        and bluetoothTagData[i]['BT_TAG_OWNER'] != bluetoothTagData[i+1]['BT_TAG_OWNER']):
                        print("X_1 : " + str(bluetoothTagData[i]['X_COORD']))
                        print("Y_1 : " + str(bluetoothTagData[i]['Y_COORD']))
                        print("X_2 : " + str(bluetoothTagData[i+1]['X_COORD']))
                        print("Y_2 : " + str(bluetoothTagData[i+1]['Y_COORD']))
                        print("TEST_BT_TAG_1 : " + str(bluetoothTagData[i]['BT_TAG_DEVICE_NAME']) + ",   TEST_BT_TAG_2 : " + str(bluetoothTagData[i+1]['BT_TAG_DEVICE_NAME']))
                        print("TEST_USER_1 : " + str(bluetoothTagData[i]['BT_TAG_OWNER']) + ",   TEST_USER_2 : " + str(bluetoothTagData[i+1]['BT_TAG_OWNER']))

                        covid19StatusBluetoothTagUser1 = list(BluetoothTagUser.objects.all().filter(USER_NAME=bluetoothTagData[i]['BT_TAG_OWNER']).values('COVID_19_STATUS'))
                        covid19StatusBluetoothTagUser1String = str(dict(covid19StatusBluetoothTagUser1[0])['COVID_19_STATUS'])
                        covid19StatusBluetoothTagUser1StringLowerCase = covid19StatusBluetoothTagUser1String.lower()
                        covid19StatusBluetoothTagUser2 = list(BluetoothTagUser.objects.all().filter(USER_NAME=bluetoothTagData[i+1]['BT_TAG_OWNER']).values('COVID_19_STATUS'))
                        covid19StatusBluetoothTagUser2String = str(dict(covid19StatusBluetoothTagUser2[0])['COVID_19_STATUS'])
                        covid19StatusBluetoothTagUser2StringLowerCase = covid19StatusBluetoothTagUser2String.lower()
                        
                        x1 = bluetoothTagData[i]['X_COORD']
                        y1 = bluetoothTagData[i]['Y_COORD']
                        x2 = bluetoothTagData[i+1]['X_COORD']
                        y2 = bluetoothTagData[i+1]['Y_COORD']
                        distanceBetweenTags = calculateDistanceBetweenTwoTags(x1, y1, x2, y2)
                        print("DISTANCE_BETWEEN_TAGS (meters) : " + str(distanceBetweenTags))

                        # CASE_1 --> COVID_STATUS_USER_1 = "negative" & COVID_STATUS_USER_2 = "negative"
                        if(covid19StatusBluetoothTagUser1String.lower() == "negative" and covid19StatusBluetoothTagUser2String.lower() == "negative"):
                            contactTracingLevel = 0
                        # CASE_2 --> COVID_STATUS_USER_1 = "negative" & COVID_STATUS_USER_2 = "positive"
                        elif(covid19StatusBluetoothTagUser1String.lower() == "negative" and covid19StatusBluetoothTagUser2String.lower() == "positive"):
                            contactTracingLevel = setContractacingLevel(distanceBetweenTags)
                        # CASE_3 --> COVID_STATUS_USER_1 = "positive" & COVID_STATUS_USER_2 = "negative"
                        # CASE_4 --> COVID_STATUS_USER_1 = "positive" & COVID_STATUS_USER_2 = "positive"
                        elif(covid19StatusBluetoothTagUser1String.lower() == "positive"):
                            contactTracingLevel = setContractacingLevel(distanceBetweenTags)
                        print("CONTACT_TRACING_LEVEL : " +  str(contactTracingLevel))

                        # ADD_DATA_TO_DB [Contact]
                        contactModel = Contact(TIMESTAMP=bluetoothTagData[i]['TIMESTAMP']
                                            ,USER_1=bluetoothTagData[i]['BT_TAG_OWNER']
                                            ,COVID_19_STATUS_USER_1=covid19StatusBluetoothTagUser1StringLowerCase
                                            ,USER_2=bluetoothTagData[i+1]['BT_TAG_OWNER']
                                            ,COVID_19_STATUS_USER_2=covid19StatusBluetoothTagUser2StringLowerCase
                                            ,DISTANCE=distanceBetweenTags
                                            ,CONTACT_TRACING_LEVEL=contactTracingLevel
                        )
                        contactModel.save()
                        print('*****-----   [updateContactModel] SAVE_DATA_ON_DB[CONTACT]_LEAW    -----*****')
                        print("----------------------------------------------------------------------------------")
            
                        #SET_FIRST_INDEX_IN_BLUETOOTH_TAG_DATA
                        first_index_in_bluetooth_tag_data = bluetoothTagData_len
                        print("[updateContactModel] first_index_in_bluetooth_tag_data : "  + str(first_index_in_bluetooth_tag_data))

        except (ValueError):
            print("******* [updateContactModel] ValueError  *******")
        except (IndexError):
            print("******* [updateContactModel] IndexError  *******")
        except (UniqueViolation):
            print("******* [updateContactModel] UniqueViolation  *******")
        except (IntegrityError):
            print("******* [updateContactModel] IntegrityError  *******")

    
    print("[updateContactModel] KO TEST NOI")
    print("----------------------------------------------------------------------------------")
    
    

# @shared_task
# def updateContactModelTest():
#     print('-----*****   [updateContactModelTest] Function  *****-----')
#     bluetoothTagData = BluetoothTagData.objects.all().values('TIMESTAMP','BT_TAG_DEVICE_NAME', 'BT_TAG_OWNER','X_COORD', 'Y_COORD', 'LOCATION', 'LATITUDE_LONGTITUDE', 'FLOOR', 'ROOM')
#     bluetoothTagData_len = BluetoothTagData.objects.all().values('BT_TAG_DEVICE_NAME', 'BT_TAG_OWNER','X_COORD', 'Y_COORD').count()
#     contactTracingLevel = 0
#     print('BTD_LEN : ' + str(bluetoothTagData_len))
#     # INDEX_START : 215
#     for i in range(len(bluetoothTagData)):
#         try:
#             if(bluetoothTagData[i]['LOCATION'] == bluetoothTagData[i+1]['LOCATION'] \
#                 and bluetoothTagData[i]['LATITUDE_LONGTITUDE'] == bluetoothTagData[i+1]['LATITUDE_LONGTITUDE'] \
#                 and bluetoothTagData[i]['FLOOR'] == bluetoothTagData[i+1]['FLOOR'] \
#                 and bluetoothTagData[i]['ROOM'] == bluetoothTagData[i+1]['ROOM']):
                    
#                     if(bluetoothTagData[i]['BT_TAG_DEVICE_NAME'] != bluetoothTagData[i+1]['BT_TAG_DEVICE_NAME'] \
#                         and bluetoothTagData[i]['BT_TAG_OWNER'] != bluetoothTagData[i+1]['BT_TAG_OWNER']):
                        
#                         print("TIMESTAMP_1 : " + str(bluetoothTagData[i]['TIMESTAMP']) + ",  TIMESTAMP_2 : " + str(bluetoothTagData[i+1]['TIMESTAMP']))
#                         print("X_1 : " + str(bluetoothTagData[i]['X_COORD']) + ",  Y_1 : " + str(bluetoothTagData[i]['Y_COORD']))
#                         # print("Y_1 : " + str(bluetoothTagData[i]['Y_COORD']))
#                         print("X_2 : " + str(bluetoothTagData[i+1]['X_COORD']) + ",  Y_2 : " + str(bluetoothTagData[i+1]['Y_COORD']))
#                         # print("Y_2 : " + str(bluetoothTagData[i+1]['Y_COORD']))
#                         print("TEST_BT_TAG_1 : " + str(bluetoothTagData[i]['BT_TAG_DEVICE_NAME']) + ",   TEST_BT_TAG_2 : " + str(bluetoothTagData[i+1]['BT_TAG_DEVICE_NAME']))
#                         # print("TEST_BT_TAG_2 : " + str(bluetoothTagData[i+1]['BT_TAG_DEVICE_NAME']))
#                         print("TEST_USER_1 : " + str(bluetoothTagData[i]['BT_TAG_OWNER']) + ",   TEST_USER_2 : " + str(bluetoothTagData[i+1]['BT_TAG_OWNER']))
#                         # print("TEST_USER_2 : " + str(bluetoothTagData[i+1]['BT_TAG_OWNER']))
                        
#                         covid19StatusBluetoothTagUser1 = list(BluetoothTagUser.objects.all().filter(USER_NAME=bluetoothTagData[i]['BT_TAG_OWNER']).values('COVID_19_STATUS'))
#                         covid19StatusBluetoothTagUser1String = str(dict(covid19StatusBluetoothTagUser1[0])['COVID_19_STATUS'])
#                         covid19StatusBluetoothTagUser1StringLowerCase = covid19StatusBluetoothTagUser1String.lower()
#                         print("COVID_STATUS_USER_1 : "  +  str(dict(covid19StatusBluetoothTagUser1[0])['COVID_19_STATUS']) )
#                         covid19StatusBluetoothTagUser2 = list(BluetoothTagUser.objects.all().filter(USER_NAME=bluetoothTagData[i+1]['BT_TAG_OWNER']).values('COVID_19_STATUS'))
#                         covid19StatusBluetoothTagUser2String = str(dict(covid19StatusBluetoothTagUser2[0])['COVID_19_STATUS'])
#                         covid19StatusBluetoothTagUser2StringLowerCase = covid19StatusBluetoothTagUser2String.lower()
#                         print("COVID_STATUS_USER_2 : " + str(dict(covid19StatusBluetoothTagUser2[0])['COVID_19_STATUS']))
                        
#                         x1 = bluetoothTagData[i]['X_COORD']
#                         y1 = bluetoothTagData[i]['Y_COORD']
#                         x2 = bluetoothTagData[i+1]['X_COORD']
#                         y2 = bluetoothTagData[i+1]['Y_COORD']
#                         distanceBetweenTags = calculateDistanceBetweenTwoTags(x1, y1, x2, y2)
#                         print("DISTANCE_BETWEEN_TAGS (meters) : " + str(distanceBetweenTags))

#                         # CASE_1 --> COVID_STATUS_USER_1 = "negative" & COVID_STATUS_USER_2 = "negative"
#                         if(covid19StatusBluetoothTagUser1String.lower() == "negative" and covid19StatusBluetoothTagUser2String.lower() == "negative"):
#                             contactTracingLevel = 0
#                         # CASE_2 --> COVID_STATUS_USER_1 = "negative" & COVID_STATUS_USER_2 = "positive"
#                         elif(covid19StatusBluetoothTagUser1String.lower() == "negative" and covid19StatusBluetoothTagUser2String.lower() == "positive"):
#                             contactTracingLevel = setContractacingLevel(distanceBetweenTags)
#                         # CASE_3 --> COVID_STATUS_USER_1 = "positive" & COVID_STATUS_USER_2 = "negative"
#                         # CASE_4 --> COVID_STATUS_USER_1 = "positive" & COVID_STATUS_USER_2 = "positive"
#                         elif(covid19StatusBluetoothTagUser1String.lower() == "positive"):
#                             contactTracingLevel = setContractacingLevel(distanceBetweenTags)
#                         print("CONTACT_TRACING_LEVEL : " +  str(contactTracingLevel))

#                         # ADD_DATA_TO_DB [Contact]
#                         contactModel = Contact(TIMESTAMP=bluetoothTagData[i]['TIMESTAMP'] 
#                                             ,USER_1=bluetoothTagData[i]['BT_TAG_OWNER']
#                                             ,COVID_19_STATUS_USER_1=covid19StatusBluetoothTagUser1StringLowerCase
#                                             ,USER_2=bluetoothTagData[i+1]['BT_TAG_OWNER']
#                                             ,COVID_19_STATUS_USER_2=covid19StatusBluetoothTagUser2StringLowerCase
#                                             ,CONTACT_TRACING_LEVEL=contactTracingLevel
#                                         )
#                         contactModel.save()

                        
        
#         except (ValueError, IndexError):
#             break

#         print("STATUS_INDEX : " + str(i))
#         print("----------------------------------------------------")
        
#     print("KO TEST NOI")



# @shared_task
def getDataFromSCADAServer():
    print("TEST :::")
    websocket.enableTrace(True)
    token = "5d9912e0dcf9cd0c136144910b293bd1ec6e844e"
    auth = "Sec-WebSocket-Protocol: " + token
    ws = websocket.WebSocket()
    ws.connect("ws://192.168.4.19/ws/tag/1/", header=[auth])
        
                
    
    # print("Sending 'Hello, World'...")
    # ws.send("Hello, World")
    print("Sent")
    print("Receiving...")
    # result =  ws.recv()
    # print("Received '%s'" % result)
    ws.close()    