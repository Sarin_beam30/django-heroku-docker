# Serializer ---> convert model instances to JSON
from django.db.models import fields
from rest_framework import serializers
from .models import BluetoothTagData, Contact
from .tasks import lower_keys

class BluetoothTagDataSerializer(serializers.ModelSerializer):
    # bt_tag_id = serializers.CharField(source='BT_TAG_ID')
    bt_tag_device_name = serializers.CharField(source='BT_TAG_DEVICE_NAME')
    bt_tag_owner = serializers.CharField(source='BT_TAG_OWNER')
    timestamp = serializers.CharField(source='TIMESTAMP')
    x_coord = serializers.CharField(source='X_COORD')
    y_coord = serializers.CharField(source='Y_COORD')
    location = serializers.CharField(source='LOCATION')
    latitude_longtitude = serializers.CharField(source='LATITUDE_LONGTITUDE')
    floor = serializers.CharField(source='FLOOR')
    room = serializers.CharField(source='ROOM')

    class Meta:
        model = BluetoothTagData
        fields = ('bt_tag_device_name', 'bt_tag_owner', 'timestamp', 'x_coord', 'y_coord', 'location', 'latitude_longtitude', 'floor', 'room')


class ContactSerializer(serializers.ModelSerializer):
    timestamp = serializers.CharField(source='TIMESTAMP')
    user_1 = serializers.CharField(source='USER_1')
    user_2 = serializers.CharField(source='USER_2')
    contact_tracing_level = serializers.CharField(source='CONTACT_TRACING_LEVEL')

    class Meta:
        model = Contact
        fields = ('timestamp', 'user_1', 'user_2', 'contact_tracing_level')