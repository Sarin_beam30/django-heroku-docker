#DJANGO
from django.db.models.expressions import Value
from django.db.models import F
from django.http.response import JsonResponse
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.contrib.gis.geos import GEOSGeometry, Point
from .models import BluetoothTagData, Contact, BluetoothTagUser
from .serializers import BluetoothTagDataSerializer, ContactSerializer
from .tasks import JsonEncoder, lower_keys
from django.utils.dateparse import parse_datetime
from datetime import datetime, timedelta
from senior_software_project_backend_side import serializers

#REST_FRAMEWORK
from rest_framework import status, generics
from rest_framework.decorators import api_view
from rest_framework.response import Response

#EXTERNAL_LIB
import threading, websocket, json, uuid, requests, datetime
from secrets import *

#CHANNEL
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync



current_user = {}

'''
CLASS 
'''
# APIView --> This provides methods handler for http verbs: get, post, put, patch, and delete
# (GenericAPIView)APIView --> Gives you shortcuts that map closely to your database models. 
#       Adds commonly required behaviour for standard list and detail views

class BluetoothTagDataSerializerView (generics.ListAPIView):
    queryset = BluetoothTagData.objects.all()
    serializer_class = BluetoothTagDataSerializer

class ContactSerializerView (generics.ListAPIView):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

'''
FUNCTION
'''
def index(request):
    # add_data_to_database.delay()
    return HttpResponse("Hello, world. You're at the main page.") 

def getAllCurrentUser(request):
    print("CURRENT_USER_FUNCTION : ", current_user)
    return JsonResponse(current_user)

def queryUsernameFromToken(token):
    global usernameFromToken
    usernameFromToken = ""
    # FIND_USERNAME_FROM_TOKEN
    if current_user != None:
        for key, value in current_user.items():
            if(key == token):
                usernameFromToken = value
    return usernameFromToken

def removeDuplicateValueInList(dataList):
    locationList = []
    uniqueLocationList = []
    newDataList = []
    for data in dataList:
        locationList.append(data["LOCATION"])
    used = set()
    uniqueLocationList = [x for x in locationList if x not in used and (used.add(x) or True)]
    # print(uniqueLocationList)

    timeStampDict = {}
    for data in dataList:
        for i in range(len(uniqueLocationList)):
            if(uniqueLocationList[i] == data["LOCATION"]):
                timeStampDict[uniqueLocationList[i]] = data
    # print(timeStampDict)

    for value in timeStampDict.values():
        newDataList.append(value)
    print(newDataList)
    return newDataList


'''
GET AND POST METHOD
'''
@api_view(['GET', 'POST'])
def add_location_to_database(request):
    if request.method == 'GET':
        data = request.data
        print("DATA : " + str(data))
        print('--- GET DATA FROM RAS_PI leaw ---')
        return Response(status=status.HTTP_200_OK)
    
    elif request.method == 'POST':
        data_from_board_string = request.data
        data_from_board_json = json.loads(data_from_board_string)

        # ADD_DATA_TO_DB [BluetoothTagData]
        # btMacAddress = data_from_board_json['BT_MAC_ADDR']
        btTagDeviceName = data_from_board_json['BT_TAG_DEVICE_NAME']
        btTagOwner = data_from_board_json['BT_TAG_OWNER']
        date_and_time = timezone.now()
        x_coordinate = data_from_board_json['X_COORD']
        y_coordinate = data_from_board_json['Y_COORD']
        location = data_from_board_json['LOCATION']
        latitude = data_from_board_json['LATITUDE']
        longtitude = data_from_board_json['LONGTITUDE']
        floor = data_from_board_json['FLOOR']
        room = data_from_board_json['ROOM']

        print("[LATITUDE] : ", latitude)
        print("[LONGTITUDE] : ", longtitude)
        
        # bt_model = BluetoothModel(Date=date_and_time, Device_name=device_name, Location=location, Floor=floor, Room=room, Latitude_Longtitude=Point(latitude, longtitude, srid=4326), X_Coordinate=x_coordinate, Y_Coordinate=y_coordinate)
        bt_model = BluetoothTagData(BT_TAG_DEVICE_NAME=btTagDeviceName, BT_TAG_OWNER=btTagOwner, TIMESTAMP=date_and_time, X_COORD=x_coordinate, Y_COORD=y_coordinate, LOCATION=location, LATITUDE_LONGTITUDE=Point(x=longtitude, y=latitude, srid=4326), FLOOR=floor, ROOM=room)
        bt_model.save()
        print('******------------ SAVE_DATA_ON_(BLTD)_DB_LEAW ------------******')
        
        return Response(str(data_from_board_string), status=status.HTTP_200_OK)
        # return Response(status=status.HTTP_200_OK)

@api_view(['GET', 'POST'])
def updateUserInformation(request):
    count_change = 0
    # userNameUpdateStatus = 0
    # POST_METHOD
    if request.method == 'POST':
        # GET_UPDATED_DATA
        data_from_frontend_json = request.data
        print("(FRONTEND_UPDATED_PROFILE) ALL DATA FROM FRONTEND : ", data_from_frontend_json)
        # token = data_from_frontend_json["token"]
        # print("(FRONTEND_UPDATED_PROFILE) TOKEN : ", token)
        userId = data_from_frontend_json["userID"]
        # print("(FRONTEND_UPDATED_PROFILE) userID : ", userId)
        newFirstName = data_from_frontend_json["firstname"]
        # print("(FRONTEND_UPDATED_PROFILE) newFirstName : ", newFirstName)
        newLastName = data_from_frontend_json["lastname"]
        # newUserName = data_from_frontend_json["username"]
        newPassword = data_from_frontend_json["password"]
        newDateOfBirth = data_from_frontend_json["dateOfBirth"]
        # print("(FRONTEND_UPDATED_PROFILE) newDateOfBirth : ", newDateOfBirth)
        newGender = data_from_frontend_json["gender"]
        newCovid19Status = data_from_frontend_json["covid19Status"]
        newHomeAddr = data_from_frontend_json["homeAddr"]
        newImageProfile = data_from_frontend_json["newImage"]
        # print("(FRONTEND_UPDATED_PROFILE) newIamge : ", newImageProfile)
        # newBluetoothMacAddr = data_from_frontend_json["btMacAddr"]
        

        # UPDATE_PROFILE_BY_USING_USER_ID
        userAllData = BluetoothTagUser.objects.filter(USER_ID=userId).values()
        print("(FRONTEND_UPDATED_PROFILE) USER_DATA : ", userAllData)

        # OLD_FIRST_NAME
        oldFirstName = BluetoothTagUser.objects.filter(USER_ID=userId).values('FIRST_NAME').first()['FIRST_NAME']

        # OLD_LAST_NAME
        oldLastName = BluetoothTagUser.objects.filter(USER_ID=userId).values('LAST_NAME').first()['LAST_NAME']

        # OLD_USER_NAME
        oldUserName = BluetoothTagUser.objects.filter(USER_ID=userId).values('USER_NAME').first()['USER_NAME']

        # OLD_PASSWORD
        oldPassword = BluetoothTagUser.objects.filter(USER_ID=userId).values('PASSWORD').first()['PASSWORD']

        # OLD_DATE_OF_BIRTH
        oldDateOfBirth = BluetoothTagUser.objects.filter(USER_ID=userId).values_list('DATE_OF_BIRTH').first()[0]
        oldDateOfBirth = str(oldDateOfBirth)

        # OLD_GENDER
        oldGender = BluetoothTagUser.objects.filter(USER_ID=userId).values('GENDER').first()['GENDER']

        # OLD_COVID_19_STATUS
        oldCovid19Status = BluetoothTagUser.objects.filter(USER_ID=userId).values('COVID_19_STATUS').first()['COVID_19_STATUS']

        # OLD_HOME_ADDR
        oldHomeAddr = BluetoothTagUser.objects.filter(USER_ID=userId).values('HOME_ADDR').first()['HOME_ADDR']

        # OLD_IMAGE_PROFILE
        oldImageProfile = BluetoothTagUser.objects.filter(USER_ID=userId).values('IMAGE_PROFILE').first()['IMAGE_PROFILE']

        # OLD_MAC_ADDR
        # oldBluetoothMacAddr = BluetoothTagUser.objects.filter(USER_ID=userId).values('BT_MAC_ADDR').first()['BT_MAC_ADDR']

    # CASE_1 : FIRSTNAME
    if(newFirstName != oldFirstName):
        
        print("(FRONTEND_UPDATED_PROFILE) oldFirstName : ", oldFirstName)
        print("(FRONTEND_UPDATED_PROFILE) newFirstName : ", newFirstName)
        userFirstName = BluetoothTagUser.objects.get(USER_ID=userId)
        userFirstName.FIRST_NAME = newFirstName
        userFirstName.save(update_fields=['FIRST_NAME'])
        print("(FRONTEND_UPDATED_PROFILE) Firstname is updated.")
        count_change += 1
    
    # CASE_2 : LASTNAME
    if(newLastName != oldLastName):
        
        print("(FRONTEND_UPDATED_PROFILE) oldLasttName : ", oldLastName)
        print("(FRONTEND_UPDATED_PROFILE) newLastName : ", newLastName)
        userLastName = BluetoothTagUser.objects.get(USER_ID=userId)
        userLastName.LAST_NAME = newLastName
        userLastName.save(update_fields=['LAST_NAME'])
        print("(FRONTEND_UPDATED_PROFILE) Lastname is updated.")
        count_change += 1
        
    # CASE_3 : PASSWORD
    if(newPassword != oldPassword):
        
        print("(FRONTEND_UPDATED_PROFILE) oldPassword : ", oldPassword)
        print("(FRONTEND_UPDATED_PROFILE) newPassword : ", newPassword)
        userPassword = BluetoothTagUser.objects.get(USER_ID=userId)
        userPassword.PASSWORD = newPassword
        userPassword.save(update_fields=['PASSWORD'])
        print("(FRONTEND_UPDATED_PROFILE) Password is updated.")
        count_change += 1
    
    # CASE_4 : DATE_OF_BIRTH
    if(newDateOfBirth != oldDateOfBirth):
        
        print("(FRONTEND_UPDATED_PROFILE) oldDate : ", oldDateOfBirth)
        print("(FRONTEND_UPDATED_PROFILE) newDate : ", newDateOfBirth)
        newTypeDateOfBirthDateTimeField = datetime.datetime.strptime(newDateOfBirth, "%Y-%m-%d").date()
        userDateOfBirth = BluetoothTagUser.objects.get(USER_ID=userId)
        userDateOfBirth.DATE_OF_BIRTH = newTypeDateOfBirthDateTimeField
        userDateOfBirth.save(update_fields=['DATE_OF_BIRTH'])
        print("(FRONTEND_UPDATED_PROFILE) DATE_OF_BIRTH is updated.")
        count_change += 1
    
    # CASE_5 : GENDER
    if(newGender != oldGender):
        userGender = BluetoothTagUser.objects.get(USER_ID=userId)
        userGender.GENDER = newGender
        userGender.save(update_fields=['GENDER'])
        print("(FRONTEND_UPDATED_PROFILE) Gender is updated.")
        count_change += 1
    
    # CASE_6 : COVID_19_STATUS
    if(newCovid19Status != oldCovid19Status):
        userCovid19 = BluetoothTagUser.objects.get(USER_ID=userId)
        userCovid19.COVID_19_STATUS = newCovid19Status
        userCovid19.save(update_fields=['COVID_19_STATUS'])
        print("(FRONTEND_UPDATED_PROFILE) Covid19Status is updated.")
        count_change += 1
    
    # CASE_6 : HOME_ADDR
    if(newHomeAddr != oldHomeAddr):
        userHomeAddr = BluetoothTagUser.objects.get(USER_ID=userId)
        userHomeAddr.HOME_ADDR = newHomeAddr
        userHomeAddr.save(update_fields=['HOME_ADDR'])
        print("(FRONTEND_UPDATED_PROFILE) HomeAddr is updated.")
        count_change += 1

    # CASE_7 : IMAGE_PROFILE
    if(newImageProfile != ""):
        if(newImageProfile != oldImageProfile):
            userImage = BluetoothTagUser.objects.get(USER_ID=userId)
            userImage.IMAGE_PROFILE = newImageProfile
            userImage.save(update_fields=['IMAGE_PROFILE'])
            print("(FRONTEND_UPDATED_PROFILE) ImageProfile is updated.")
            count_change += 1
    
    # CASE_8 : BT_MAC_ADDR
    # if(newBluetoothMacAddr != oldBluetoothMacAddr):
    #     userBTMacAddr = BluetoothTagUser.objects.get(USER_ID=userId)
    #     userBTMacAddr.BT_MAC_ADDR = newBluetoothMacAddr
    #     userBTMacAddr.save(update_fields=['BT_MAC_ADDR'])
    #     print("(FRONTEND_UPDATED_PROFILE) BluetoothMacAddr is updated.")
    #     count_change += 1

    print("COUNT_CHANGE : " , count_change)
    if(count_change == 0) :
        return Response({"message": "Your profile did not update."})
    else :
        return Response({"message": "Your profile were update."})

@api_view(['GET', 'POST'])
def userAndAdminInformation(request):
    allUserList = []
    allUserDict = {}
    # GET_METHOD
    if request.method == "GET": 
        lenOfBluetoothTagData = BluetoothTagData.objects.all().count()       
        if(lenOfBluetoothTagData > 0):
            listUserInfoBluetoothTagUser = list(BluetoothTagUser.objects.values('FIRST_NAME', 'LAST_NAME', 'USER_NAME'))
            # print("(adminInformation) listUserInfoBluetoothTagUser : ", listUserInfoBluetoothTagUser)
            for i in range(len(listUserInfoBluetoothTagUser)):
                # Check BluetoothTagUser has BluetoothTagData or not ?
                listUserInfoBluetoothTagData= list(BluetoothTagData.objects.values('BT_TAG_OWNER'))
                for j in range(len(listUserInfoBluetoothTagData)):
                    if(listUserInfoBluetoothTagUser[i]['USER_NAME'] == listUserInfoBluetoothTagData[j]['BT_TAG_OWNER']):      
                        allUserList.append(listUserInfoBluetoothTagUser[i]['USER_NAME'])
                        break
            
            allUserDict["username"] = allUserList
            print(allUserDict)
            return Response({"message": allUserDict}) 
        elif(lenOfBluetoothTagData <= 0):
            return Response({"message": "No Bluetooth Tag Data"})   


        # listUserInfoBluetoothTagUser = list(BluetoothTagUser.objects.values('FIRST_NAME', 'LAST_NAME', 'USER_NAME'))
        # # print("(adminInformation) listUserInfoBluetoothTagUser : ", listUserInfoBluetoothTagUser)
        # for i in range(len(listUserInfoBluetoothTagUser)):
        #     sumTwoDict = {}
        #     # Check BluetoothTagUser has BluetoothTagData or not ?
        #     listUserInfoBluetoothTagData= list(BluetoothTagData.objects.values('BT_TAG_OWNER'))
        #     for j in range(len(listUserInfoBluetoothTagData)):
        #         if(listUserInfoBluetoothTagUser[i]['USER_NAME'] == listUserInfoBluetoothTagData[j]['BT_TAG_OWNER']):      
        #             # print("(adminInformation) listUserInfoBluetoothTagData : ", type(listUserInfoBluetoothTagData['BT_TAG_OWNER']))
        #             listBluetoothTagInfo = BluetoothTagData.objects.all()\
        #                 .filter(BT_TAG_OWNER=listUserInfoBluetoothTagUser[i]['USER_NAME'])\
        #                 .values('TIMESTAMP', 'LOCATION', 'LATITUDE_LONGTITUDE')\
        #                 .latest('TIMESTAMP')
        #             # print("(adminInformation) listBluetoothTagInfo : ", listBluetoothTagInfo)
        #             listBluetoothTagInfoString = json.dumps(listBluetoothTagInfo, cls=JsonEncoder)
        #             listBluetoothTagInfoJson = json.loads(listBluetoothTagInfoString)
        #             sumTwoDict = listUserInfoBluetoothTagUser[i].copy()
        #             sumTwoDict.update(listBluetoothTagInfoJson)
        #             sumTwoDict = lower_keys(sumTwoDict)
        #             allUserList.append(sumTwoDict)
        #             break
                    
        # print("(adminInformation) allUserList : ", allUserList)
        # return Response({"message": allUserList})
    
    # POST_METHOD
    elif request.method == "POST":
        # GET_TOKEN_FROM_FRONTEND
        data_from_frontend_json = request.data
        print("(FRONTEND_PROFILE) DATA : ", (data_from_frontend_json))
        if(data_from_frontend_json == {}):
            return Response({"message": "Please use POST method with user's token"})
        elif (data_from_frontend_json != {}):
            token = data_from_frontend_json["token"]
            usernameFromToken = queryUsernameFromToken(token)
            print("(FRONTEND_PROFILE) USERNAME_FROM_TOKEN : ", usernameFromToken)
            # QUERY_USER_INFORMATION
            userData = BluetoothTagUser.objects.filter(USER_NAME=usernameFromToken).values()
            # print("(FRONTEND_PROFILE) USER_DATA : ", userData)
            return Response({"message": "Get your userInformation", "userInformation": userData})

@api_view(['GET', 'POST'])
def userRegistration(request):
    # INIT_VARIABLE
    userExistOrNot = False
    if request.method == 'GET':
        print(" --- (REGISTRATION) GET_METHOD --- ")
        return Response(status=status.HTTP_200_OK)

    elif request.method == 'POST':
        # GET_DATA
        data_from_frontend_json = request.data
        print("(FRONTEND_REGISTRATION) DATA : ", (data_from_frontend_json))
        btMacAddr = data_from_frontend_json["btMacAddr"]
        firstname = data_from_frontend_json["firstname"]
        lastname = data_from_frontend_json["lastname"]
        username = data_from_frontend_json["username"]
        password = data_from_frontend_json["password"]
        dateOfBirth = data_from_frontend_json["dateOfBirth"]
        gender = data_from_frontend_json["gender"]
        homeAddr = data_from_frontend_json["homeAddr"]

        # CONVERT DATE_STRING TO DJANGO_DATETIME_FIELD 
        # dateOfBirthDateTimeField = parse_datetime(dateOfBirth)
        dateOfBirthDateTimeField = datetime.datetime.strptime(dateOfBirth, "%m/%d/%Y").date()
        # print("BIRTH_DATE : " , dateOfBirthDateTimeField)

        #CHECK_THE_EXISTING_USER
        userData = BluetoothTagUser.objects.all().values('FIRST_NAME', 'LAST_NAME', 'USER_NAME', 'PASSWORD')
        # userData = BluetoothTagUser.objects.all().values('FIRST_NAME', 'LAST_NAME', 'USER_NAME', 'PASSWORD', 'BT_MAC_ADDR')
        # print(userData)
        # userDataLen = User.objects.all().values('NAME', 'USERNAME', 'PASSWORD').count()
        for i in range(len(userData)):
            # print("---- **** (FRONTEND_REGISTRATION) KO TEST NOI  **** ----")
            # CHECK FIRST_NAME , LAST_NAME AND USERNAME
            if(userData[i]['FIRST_NAME'] == firstname and userData[i]['LAST_NAME'] == lastname and userData[i]['USER_NAME'] == username):
                print("User already exist in the system.")
                userExistOrNot = True
            # elif(userData[i]['BT_MAC_ADDR'] == btMacAddr):
            #     print("This mac address is sign already in the system.") 
            #     return Response({"message": "This bluetooth mac address already exist in the system."})


        if (userExistOrNot == True):
            print("User exist == true")  
            return Response({"message": "User already exist in the system."})
        else:
            print("User exist == false")      
            # ADD_TO_DB
            # user_model = BluetoothTagUser(FIRST_NAME=firstname, LAST_NAME=lastname, USER_NAME=username, PASSWORD=password, \
            #     DATE_OF_BIRTH=dateOfBirthDateTimeField, GENDER=gender, HOME_ADDR=homeAddr, BT_MAC_ADDR=btMacAddr)
            user_model = BluetoothTagUser(FIRST_NAME=firstname, LAST_NAME=lastname, USER_NAME=username, PASSWORD=password, \
                DATE_OF_BIRTH=dateOfBirthDateTimeField, GENDER=gender, HOME_ADDR=homeAddr)
            user_model.save()
            print('*****------    SAVE_DATA_ON_(BLUETOOTH_TAG_USER)_DB_LEAW    ------*****')
            return Response({"message": "New User and Got some data!", "data": request.data}) 

@api_view(['POST'])
def userLogin(request):
    index_user = 0   
    userIsInSystemOrNot = False
    userIsLoginAlready = False
    global current_user
    if request.method == 'POST':
        # GET_USERNAME_&&_PASSWORD_FROM_FRONTEND
        data_from_frontend_json = request.data
        print("(FRONTEND_LOGIN) DATA : ", (data_from_frontend_json))

        if(data_from_frontend_json == {}):
            return Response({"message": "username and password not found"})
        elif(data_from_frontend_json != {}):
            username = data_from_frontend_json["username"]
            password = data_from_frontend_json["password"]

            # ADMIN_LOGIN
            if(username.upper() == 'ADMIN'):
                print("You login as an admin.")
                return Response({"message": "You login as an admin.", "allUserData": "TEST"})

            # CHECK USERNAME AND PASSWORD ARE CORRECT OR NOT 
            userData = BluetoothTagUser.objects.all().values('DATE_OF_BIRTH', 'FIRST_NAME', \
                'LAST_NAME', 'GENDER', 'HOME_ADDR','USER_NAME', 'PASSWORD')

            for i in range(len(userData)):
                # print("---- **** (FRONTEND_LOGIN) KO TEST NOI  **** ----")
                #CASE_1 : USERNAME && PASSWORD --> WRONG
                #CASE_2 : USERNAME OR PASSWORD --> WRONG
                #CASE_3 : USERNAME && PASSWORD --> CORRECT
                if(userData[i]['USER_NAME'] == username and userData[i]['PASSWORD'] == password):
                    index_user = i
                    userIsInSystemOrNot = True
            
            # CHECK USER IS IN SYSTEM OR NOT
            if(userIsInSystemOrNot == True):
                print("User is in the system == TRUE")

                # CHECK USER IS LOGIN ALREADY OR NOT
                print("current_user.values [USERNAME] : " , current_user.values())
                
                for key, value in current_user.items():
                    if value == username:
                        userIsLoginAlready = True
                
                if(userIsLoginAlready == True):
                    print("(FRONTEND_LOGIN) User already login in the system.")
                    return Response({"message": "User already login in the system."})

                elif(userIsLoginAlready == False):
                    #GENERATE_TOKEN_TO_EACH_USER
                    token = token_urlsafe(16)

                    # UPDATE_CURRENT_USER_JSON_VARIABLE
                    # print("user_with_token : ", user_with_token)
                    current_user[token] = userData[index_user]['USER_NAME']
                    print("(FRONTEND_LOGIN) USER_NAME : ", userData[index_user]['USER_NAME'])
                    # print("(FRONTEND_LOGIN) USER_ID : ", userData[index_user]['USER_ID'])
                    print("(FRONTEND_LOGIN) Login Success")
                    
                    return Response({"token": token, 
                                    "dateOfBirth": userData[index_user]['DATE_OF_BIRTH'],
                                    "firstname" : userData[index_user]['FIRST_NAME'],
                                    "lastname" : userData[index_user]['LAST_NAME'],
                                    "gender" : userData[index_user]['GENDER'],
                                    "homeAddr" : userData[index_user]['HOME_ADDR'],
                                    "username": userData[index_user]['USER_NAME'],
                                    "password" : userData[index_user]['PASSWORD'] })
                
            else:
                print("User is in the system == FALSE \nYou put wrong either username or password.")
                return Response({"message": "You put wrong either username or password."})

@api_view(['POST'])
def userLogout(request):
    global current_user
    if request.method == 'POST':
        data_from_frontend = request.data
        print("(USER_LOGOUT) DATA : ", data_from_frontend)
        # print("(USER_LOGOUT) TYPE : ", type(data_from_frontend))

        if("token" not in data_from_frontend):
            return Response({"message": "Token Not Found"})
        elif("token" in data_from_frontend):
            token = data_from_frontend["token"]
            if current_user != {}:            
                try:
                    print("(FRONTEND_LOGOUT) TOKEN : ", token)
                    current_user.pop(token)
                    print("(FRONTEND_LOGOUT) Logout Success")
                    return Response({"message": "Logout Success"})
                except KeyError:
                    print("(FRONTEND_LOGOUT) User already Logout")
                    return Response({"message": "User already logout"})
                
            elif current_user == {}:
                print("(FRONTEND_LOGOUT) No user in the system")
                return Response({"message": "No user in the system"})

@api_view(['POST'])
def userOutdoor(request):
    username = ""

    # POST_METHOD
    if request.method == "POST":
        # GET_TOKEN_FROM_FRONTEND
        data_from_frontend_json = request.data
        print("(OUTDOOR_LOCATION_SERVICE) DATA : ", (data_from_frontend_json))

        # CASE_1 : USE TOKEN
        if("token" in data_from_frontend_json):
            token = data_from_frontend_json["token"]
            username = queryUsernameFromToken(token)
            print("(OUTDOOR_LOCATION_SERVICE) USERNAME_FROM_TOKEN : ", usernameFromToken)
        # CASE_2 : USE USERNAME
        elif("username" in data_from_frontend_json):
            username = data_from_frontend_json["username"]
            if(username == "" or username == " "):
                return Response({"message":"Username Not Found"})
        else:
            return Response({"message": "userOutdoor Information not found"})

        # QUERY_USER_INFORMATION
        userDataObject = list(BluetoothTagData.objects.filter(BT_TAG_OWNER=username).values('TIMESTAMP', 'LOCATION', 'LATITUDE_LONGTITUDE'))
        # print("(OUTDOOR_LOCATION_SERVICE) userDataObject : ", userDataObject)
        userDataString = json.dumps(userDataObject, cls=JsonEncoder)
        # print("(OUTDOOR_LOCATION_SERVICE) userDataString : ", userDataString)
        userDataJson = json.loads(userDataString)
        # print("(OUTDOOR_LOCATION_SERVICE) userDataJson : ", userDataJson)
        userDataJsonNewVersion = removeDuplicateValueInList(userDataJson)
        # print("(OUTDOOR_LOCATION_SERVICE) userDataJsonNewVersion : ", userDataJsonNewVersion)
        userDataJsonNewVersionLowerCase = lower_keys(userDataJsonNewVersion)
        print("(OUTDOOR_LOCATION_SERVICE) USER_DATA_JSON : ", userDataJsonNewVersionLowerCase)
        return Response({"message": userDataJsonNewVersionLowerCase})

'''
GET METHOD 
'''


'''
POST METHOD
'''
# @api_view(['POST'])
# def getLocationFromArduino(request):
#     data = request.data
#     print(data)
