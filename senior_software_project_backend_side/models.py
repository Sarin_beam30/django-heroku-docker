from enum import unique
from datetime import datetime, date
import django, uuid
from django.contrib.gis.db import models
from django.conf import settings
from django.utils import timezone
from django.contrib.gis.geos import GEOSGeometry, Point


class BluetoothTagUser(models.Model):
    # USER_ID = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)
    USER_ID = models.TextField(blank=False, default=uuid.uuid4, editable=False)
    # BT_MAC_ADDR = models.CharField(blank=False, default="99:99:99:99", max_length=70)
    IMAGE_PROFILE = models.CharField(blank=False, default="defaultProfilePicture.jpg", max_length=100)
    DATE_OF_BIRTH = models.DateField(blank=False, default=date.today)
    FIRST_NAME = models.CharField(blank=False, default='YOUR_FIRST_NAME', max_length=20)
    LAST_NAME = models.CharField(blank=False, default='YOUR_LAST_NAME', max_length=20)
    GENDER = models.CharField(blank=False, default='YOUR_GENDER', max_length=20)
    HOME_ADDR = models.TextField(blank=False, default='YOUR_HOME_ADDR')
    USER_NAME = models.CharField(blank=False, default='USER_NAME', max_length=20)
    PASSWORD = models.CharField(blank=False, default='TEST1234', max_length=20)
    COVID_19_STATUS = models.CharField(blank=False, default='negative', max_length=20)
    COVID_19_LINEAGE = models.CharField(blank=False, default='[UK] B.1.1.7', max_length=30)
    # Medical_certificate_image 

    #  note (COVID_19 : lineage)
    # 1) Lineage B.1617.2 (INDIA) (LEVEL : 1)
    # 2) Lineage B.1.1.7 (UK)(Varient of Concern in Dec-2020) (LEVEL : 2)
    # 3) Lineage B.1.351 (South Africa) (LEVEL : 3)
    # 4) Lineage P.1 (Brazil & Japan)(Amazon rain forest) (LEVEL : 4)
    
    
    # USER_CONTACT_TRACING_LEVEL = models.IntegerField(default=1)
    # CONTACT_TRACING_LIST = models.TextField(default='BT_TAG_001, 25m')
    
    def __str__(self):
        return "%s" % self.USER_NAME

class Contact(models.Model):
    ID = models.AutoField(primary_key=True)
    TIMESTAMP = models.DateTimeField(default=timezone.now)
    # BT_TAG_1 = models.ForeignKey(User, verbose_name="BT_TAG_1", on_delete=models.CASCADE)
    USER_1 = models.TextField(default='sarin_beam30')
    COVID_19_STATUS_USER_1 = models.TextField(default='positive')
    USER_2 = models.TextField(default='window_1234')
    COVID_19_STATUS_USER_2 = models.TextField(default='positive')
    DISTANCE = models.FloatField(default=1.234)
    CONTACT_TRACING_LEVEL = models.IntegerField(default=0)

    # class Meta:
    #     unique_together = [['TIMESTAMP', 'USER_1', 'USER_2', 'CONTACT_TRACING_LEVEL']]
    

    def __str__(self):
        return "%s" % self.ID

class BluetoothTagData(models.Model):
    # TIME_CONFIG
    # mytime = datetime.now()
    # mytime_format = mytime.strftime("%d-%m-%Y %H:%M:%S")
    
    BT_TAG_ID = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)
    # BT_MAC_ADDR = models.CharField(default="99:99:99:99", max_length=70)
    BT_TAG_DEVICE_NAME = models.CharField(default='BT_0001', max_length=20)
    # BT_TAG_OWNER = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=False, default=uuid.uuid4)
    BT_TAG_OWNER = models.CharField(default='sarin_beam30', max_length=20)
    TIMESTAMP = models.DateTimeField(default=django.utils.timezone.now)
    # X_&_Y_COORD use srid = 3857
    X_COORD = models.FloatField(default=1.234)
    Y_COORD = models.FloatField(default=5.678)
    
    #SRID=4326; POINT(-80.11 25.11)
    #Point(longitude, latitude, srid=4326)
    LOCATION = models.CharField(default='ECC Building', max_length=50)
    LATITUDE_LONGTITUDE = models.PointField(geography=True, default=Point(1.234, 5.678) )
    # LONGTITUDE = models.PointField(srid=4326)
    FLOOR = models.IntegerField(default=1)
    ROOM =  models.CharField(default='ECC-707', max_length=20)

    def __str__(self):
        return "%s" % self.BT_TAG_DEVICE_NAME

