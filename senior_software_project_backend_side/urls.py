from django.urls import path
from . import views
# path(route=contain a URL pattern, views=view_function, name=URL_name)

urlpatterns = [
    path('', views.index, name='index'),
    path('API/location/', views.BluetoothTagDataSerializerView.as_view()),
    path('API/contact/', views.ContactSerializerView.as_view()),
    path('getLocation/', views.add_location_to_database, name='add_location_to_database'),
    path('userRegistration/', views.userRegistration, name='userRegistration'),
    path('userLogin/', views.userLogin),
    path('userLogout/', views.userLogout),
    path('userAndAdminInformation/', views.userAndAdminInformation),
    path('userOutdoor/', views.userOutdoor),
    path('updateUserInformation/', views.updateUserInformation),
    path('getAllCurrentUser/', views.getAllCurrentUser),
]
