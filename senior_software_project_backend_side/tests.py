from django.contrib.gis.geos.point import Point
from django.test import TestCase
import pytest
from channels.testing import WebsocketCommunicator
from rest_framework import response
from senior_software_project_backend_side.consumers import ContactConsumer, RealtimeLocationConsumer
from senior_software_project_backend_side.models import BluetoothTagData, BluetoothTagUser
from datetime import date, datetime

# TEST_REST_API
class RestAPITest(TestCase):
    def testGETMethodForAPILocationPath(self):
        response = self.client.get("https://protected-brook-89084.herokuapp.com/API/location/")
        self.assertEqual(response.status_code, 200)
    
    def testPOSTMethodForAPILocationPath(self):
        response = self.client.post("https://protected-brook-89084.herokuapp.com/API/location/")
        self.assertEqual(response.status_code, 405)
    
    def testGETMethodForUserAndAdminInformationPath(self):
        response = self.client.get("https://protected-brook-89084.herokuapp.com/userAndAdminInformation/")
        self.assertEqual(response.status_code, 200)
    
    def testPOSTMethodForUserAndAdminInformationPath(self):
        response = self.client.post("https://protected-brook-89084.herokuapp.com/userAndAdminInformation/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"message":"Please use POST method with user\'s token"}' )
    
    def testPUTMethodForUserAndAdminInformationPath(self):
        response = self.client.put("https://protected-brook-89084.herokuapp.com/userAndAdminInformation/")
        self.assertEqual(response.status_code, 405)
    
    def testPOSTMethodForUserLoginPath(self):
        response = self.client.post("https://protected-brook-89084.herokuapp.com/userLogin/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"message":"username and password not found"}' )
    
    def testGETMethodForUserLoginPath(self):
        response = self.client.get("https://protected-brook-89084.herokuapp.com/userLogin/")
        self.assertEqual(response.status_code, 405)
    
    def testPostMethodForUserLogoutPath(self):
        response = self.client.post("https://protected-brook-89084.herokuapp.com/userLogout/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"message":"Token Not Found"}' )
    def testGetMethodForUserLogoutPath(self):
        response = self.client.get("https://protected-brook-89084.herokuapp.com/userLogout/")
        self.assertEqual(response.status_code, 405)

# TEST_WEB_SOCKET_API
class WebSocketTest(TestCase):
    @pytest.mark.asyncio
    async def testRealTimeLocationConsumer(self):
        communicator = WebsocketCommunicator(RealtimeLocationConsumer.as_asgi(), "ws/location/")
        connected, subprotocol = await communicator.connect()
        assert connected

        # Test sending text_1
        await communicator.send_json_to({"payload" : "hello_from_realTimeLocationConsumer_1"})
        response = await communicator.receive_json_from()
        assert response == {"payload" : "hello_from_realTimeLocationConsumer_1"}

        # Test sending text_2
        await communicator.send_json_to({"payload" : "hello_from_realTimeLocationConsumer_2"})
        response = await communicator.receive_json_from()
        assert response == {"payload" : "hello_from_realTimeLocationConsumer_2"}

        # Close
        await communicator.disconnect()

    @pytest.mark.asyncio
    async def testContactConsumer(self):
        communicator = WebsocketCommunicator(ContactConsumer.as_asgi(), "ws/contact/")
        connected, subprotocol = await communicator.connect()
        assert connected
        
        # Test sending text_1
        await communicator.send_json_to({"contact_payload" : "hello_from_contactConsumer_1"})
        response = await communicator.receive_json_from()
        assert response == {"contact_payload" : "hello_from_contactConsumer_1"}
        
        # Test sending text_2
        await communicator.send_json_to({"contact_payload" : "hello_from_contactConsumer_2"})
        response = await communicator.receive_json_from()
        assert response == {"contact_payload" : "hello_from_contactConsumer_2"}

        # Close
        await communicator.disconnect()

# TEST_DATABASE
class BluetoothTagUserModelTest(TestCase):
    # Create BluetoothTagUser Object
    def setupTestData(self):
        # print("\nSANG LEAW NA")
        return BluetoothTagUser.objects.create(DATE_OF_BIRTH=date.today(), FIRST_NAME='Big', LAST_NAME='Bob', GENDER='Male', \
            USER_NAME='bigBob', PASSWORD='Test1234')

    # Check Type of a new BluetoothTagUser object is a type of BluetoothTagUser model or not
    # Check returned username form ___str__() which is equivalent to object of BluetoothTagUser.USER_NAME
    def testTypeOfNewBluetoothTagUserObject(self):
        bluetoothTagUser = self.setupTestData()
        # assertTrue = Check the given argument is true or not
        # isintance = Check the specified object is of the specified type
        self.assertTrue(isinstance(bluetoothTagUser, BluetoothTagUser))
        # assertEqual = Check two given parameter is equivalent or not
        # self.assertEqual(bluetoothTagUser.__str__(), bluetoothTagUser.USER_NAME)

    # Test type of each parameter of a new BTU(BluetoothTagUser) object is a string type or not
    def testInformationFields(self):
        bluetoothTagUser = self.setupTestData()
        self.assertIsInstance(bluetoothTagUser.DATE_OF_BIRTH, date)
        self.assertIsInstance(bluetoothTagUser.FIRST_NAME, str)
        self.assertIsInstance(bluetoothTagUser.LAST_NAME, str)
        self.assertIsInstance(bluetoothTagUser.GENDER, str)
        self.assertIsInstance(bluetoothTagUser.HOME_ADDR, str)
        self.assertIsInstance(bluetoothTagUser.USER_NAME, str)
        self.assertIsInstance(bluetoothTagUser.PASSWORD, str)

class BluetoothTagDataModelTest(TestCase):
    # Create BluetoothTagData Object
    # FIRST_CASE : BT_TAG_OWNER
    def setupDataSet1(self):
        return BluetoothTagData.objects.create(BT_TAG_DEVICE_NAME="IPHONE_12", BT_TAG_OWNER=1,
        TIMESTAMP=datetime.now(), X_COORD=1.234, Y_COORD=5.678, LOCATION="My dormitory",
        LATITUDE_LONGTITUDE=Point(x=100.01, y=500.01, srid=4326), FLOOR=8, ROOM="HM-123")
    
    # SECOND_CASE : TIMESTAMP
    def setupDataSet2(self):
        return BluetoothTagData.objects.create(BT_TAG_DEVICE_NAME="IPHONE_12", BT_TAG_OWNER="sarin_beam30",
        TIMESTAMP="20-May-2021 16:44", X_COORD=1.234, Y_COORD=5.678, LOCATION="My dormitory",
        LATITUDE_LONGTITUDE=Point(x=100.01, y=500.01, srid=4326), FLOOR=8, ROOM="HM-123")
    
    # THIRD_CASE : X_COORD && Y_COORD
    def setupDataSet3(self):
        return BluetoothTagData.objects.create(BT_TAG_DEVICE_NAME="IPHONE_12", BT_TAG_OWNER="sarin_beam30",
        TIMESTAMP=datetime.now(), X_COORD="xxxx", Y_COORD="yyyy", LOCATION="My dormitory",
        LATITUDE_LONGTITUDE=Point(x=100.01, y=500.01, srid=4326), FLOOR=8, ROOM="HM-123")
    
    # FORTH_CASE : ROOM
    def setupDataSet4(self):
        return BluetoothTagData.objects.create(BT_TAG_DEVICE_NAME="IPHONE_12", BT_TAG_OWNER="sarin_beam30",
        TIMESTAMP=datetime.now(), X_COORD=1.234, Y_COORD=5.678, LOCATION="My dormitory",
        LATITUDE_LONGTITUDE=Point(x=100.01, y=500.01, srid=4326), FLOOR=8, ROOM=1223)
    
    def testDataSet1(self):
        self.setupDataSet1()
    
    def testDataSet2(self):
        self.setupDataSet2()
    
    def testDataSet3(self):
        self.setupDataSet3()
    
    def testDataSet4(self):
        self.setupDataSet4()
    
    # def testTypeOfDataField(self):
    #     bluetoothTagData = self.setupTestData()

        
        