# Generated by Django 3.1.7 on 2021-05-21 20:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('senior_software_project_backend_side', '0007_auto_20210518_0035'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='COVID_19_STATUS_USER_1',
            field=models.TextField(default='positive'),
        ),
        migrations.AddField(
            model_name='contact',
            name='COVID_19_STATUS_USER_2',
            field=models.TextField(default='positive'),
        ),
    ]
