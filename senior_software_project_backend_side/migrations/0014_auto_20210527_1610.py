# Generated by Django 3.1.7 on 2021-05-27 16:10

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('senior_software_project_backend_side', '0013_auto_20210527_1606'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='TIMESTAMP',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
