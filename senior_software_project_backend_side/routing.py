from django.urls import re_path
from . import consumers
websocket_urlpatterns = [
    re_path(r'ws/location/$', consumers.RealtimeLocationConsumer.as_asgi()),
    re_path(r'ws/contact/$', consumers.ContactConsumer.as_asgi()),
]

