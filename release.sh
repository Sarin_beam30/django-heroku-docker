#!/bin/sh


IMAGE_ID=$(docker inspect web --format={{.Id}})
PAYLOAD='{"updates": [{"type": "web", "docker_image": "'"sha256:fb73f9f17558e0fb420194d661cc60d29c14ee08530f22375eea1175472cb544"'"}]}'

curl -n -X PATCH https://api.heroku.com/apps/protected-brook-89084/formation \
  -d "Release" \
  -H "Content-Type: application/json" \
  -H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
  -H "Authorization: Bearer 5d8ef86b-b834-45f0-a61f-f47347f625ce"


# curl --netrc -X PATCH https://api.heroku.com/apps/protected-brook-89084/formation \
#   -d '{
#   "updates": [
#     {
#       "type": "web",
#       "docker_image": "sha256:fb73f9f17558e0fb420194d661cc60d29c14ee08530f22375eea1175472cb544"
#     },
#   ]
# }' \
#   -H "Content-Type: application/json" \
#   -H "Accept: application/vnd.heroku+json; version=3.docker-releases"