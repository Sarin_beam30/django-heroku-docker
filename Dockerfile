# pull official base image
FROM python:3.8

# set work directory
WORKDIR /app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0

# install GDAL & postgreSQL
RUN apt-get update &&\
    apt-get install -y --no-install-recommends apt-utils \
    binutils libproj-dev gdal-bin python-gdal python3-gdal postgresql postgresql-contrib rabbitmq-server 

# install dependencies
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# copy project
COPY . .

# collect static files
RUN python manage.py collectstatic --noinput

# add and run as non-root user
# RUN adduser -D myuser
# USER myuser

# run gunicorn
# CMD gunicorn hello_django.wsgi:application --bind 0.0.0.0:$PORT

# run daphne
# CMD daphne hello_django.asgi:application --port $PORT --bind 0.0.0.0 -v2
# CMD daphne hello_django.asgi:application --bind 0.0.0.0:$PORT

# run worker
# CMD celery -A hello_django worker -B --pool=solo -l info
